﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsFine.WebApp.Models;

namespace SportsFine.WebApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Admin()
        {
            //User.IsInRole
            //ViewData["Message"] = "Your admin page.";

            return View();
        }

        public IActionResult CreateFine()
        {
            ViewData["Message"] = "Your fines page.";

            return View();
        }

        public IActionResult MyFines()
        {
            ViewData["Message"] = "Your fines page.";

            return View();
        }

        public IActionResult FinesList()
        {
            ViewData["Message"] = "Your fines page.";

            return View();
        }

        public IActionResult Home()
        {
            ViewData["Message"] = "Your fines page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
