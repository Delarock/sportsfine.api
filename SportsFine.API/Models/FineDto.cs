﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsFine.API.Models
{
    public class FineDto
    {
        public int FineId { get; set; }
        public int FineTypeId { get; set; }
        public int PlayerFinedId { get; set; }
        public int PlayerReportingId { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string Explaination { get; set; }

        public int Amount { get; set; }
        public DateTime Date { get; set; }
        public bool IsApproved { get; set; }
        public bool IsPaid { get; set; }
        public DateTime DatePaid { get; set; }
    }
}
