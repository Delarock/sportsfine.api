﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsFine.API.Models
{
    public class PlayerDto
    {
        public int IdPlayer { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public bool IsAdmin { get; set; }

    }
}
