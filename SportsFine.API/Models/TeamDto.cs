﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsFine.API.Models
{
    public class TeamDto
    {
        public int TeamId { get; set; } 
        public int ClubId { get; set; }
        public string Name { get; set; }

    }
}
