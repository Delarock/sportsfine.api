﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SportsFine.API.Services;
using SportsFine.API.Entities;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using SportsFine.API.Helpers;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace SportsFine.API
{
    public class Startup
    {
        public static IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
           
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
            }));

            services.AddMvc(setupAction =>
            {
                setupAction.ReturnHttpNotAcceptable = true;
                setupAction.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter()); //Doesnt seem to work
            });

            // register the DbContext on the container, getting the connection string from
            // appSettings (note: use this during development; in a production environment,
            // it's better to store the connection string in an environment variable)
            var connectionString = Configuration["connectionStrings:SportsFineDBConnectionString"];
            services.AddDbContext<SportsFineContext>(o => o.UseSqlServer(connectionString));

            // register the repository
            services.AddScoped<ISportsFineRepository, SportsFineRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            ILoggerFactory loggerFactory, SportsFineContext sportsfineContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async context =>
                    {
                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync("An unexpected fault happened. Please try again later."); 
                    });
                });
            }

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Entities.Player, Models.PlayerDto>()
                    .ForMember(dest => dest.IdPlayer, opt => opt.MapFrom(src =>
                    src.IdPlayer))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src =>
                    $"{src.FirstName} {src.LastName}"))
                    .ForMember(dest => dest.Age, opt => opt.MapFrom(src =>
                    src.DateOfBirth.GetCurrentAge()));

                cfg.CreateMap<Entities.Fine, Models.FineDto>()
                    .ForMember(dest => dest.FineId, opt => opt.MapFrom(src =>
                    src.IdFine))
                    .ForMember(dest => dest.Title, opt => opt.MapFrom(src =>
                    src.FineType.Title))
                    .ForMember(dest => dest.Description, opt => opt.MapFrom(src =>
                   src.FineType.Description))
                    .ForMember(dest => dest.Amount, opt => opt.MapFrom(src =>
                    src.FineType.Amount))
                    .ForMember(dest => dest.Explaination, opt => opt.MapFrom(src =>
                    src.Explaination))
                    .ForMember(dest => dest.Date, opt => opt.MapFrom(src =>
                    src.Date))
                    .ForMember(dest => dest.IsApproved, opt => opt.MapFrom(src =>
                    src.IsApproved))
                    .ForMember(dest => dest.IsPaid, opt => opt.MapFrom(src =>
                    src.IsPaid))
                    .ForMember(dest => dest.DatePaid, opt => opt.MapFrom(src =>
                    src.DatePaid))
                    .ForMember(dest => dest.PlayerFinedId, opt => opt.MapFrom(src =>
                    src.Player.IdPlayer));


            });

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                    serviceScope.ServiceProvider.GetService<SportsFineContext>().Database.Migrate();
                    serviceScope.ServiceProvider.GetService<SportsFineContext>().EnsureSeedDataForContext();
            }

            app.UseCors("MyPolicy");
            app.UseMvc();
        }
    }
}
