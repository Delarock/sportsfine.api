﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsFine.API.Entities
{
    public class Team
    {
        [Key]
        public int IdTeam { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        //Used for relationships
        public List<Player> Players { get; set; }

        [Required]
        public virtual Club Club { get; set; }

        
        

    }
}
