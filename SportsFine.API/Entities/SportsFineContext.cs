﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsFine.API.Entities
{
    public class SportsFineContext : DbContext
    {
        public SportsFineContext(DbContextOptions<SportsFineContext> options)
           : base(options)
        {
            Database.Migrate();
        }

        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]

        public DbSet<Fine> Fines { get; set; }
        public DbSet<FineType> FineTypes { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<Team> Teams { get; set; }

    }
}
