﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsFine.API.Entities
{
    public class Player
    {
        [Key]
        public int IdPlayer { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        public DateTimeOffset DateOfBirth { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsApproved { get; set; }
        
        //Used for relationships
        public List<Fine> Fines { get; set; }
        
        //[Required]
        public virtual Team Team { get; set; }

        //[Required]
        //public Club Club { get; set; }

    }
}
