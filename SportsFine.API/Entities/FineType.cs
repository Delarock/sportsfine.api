﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsFine.API.Entities

{
    public class FineType
    {
        [Key]
        public int IdFineType { get; set; }

        [Required]
        [MaxLength(100)]
        public string Title { get; set; }

        [Required]
        public int Amount { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        //Used for relationships
        public List<Fine> Fines { get; set; }


    }
}