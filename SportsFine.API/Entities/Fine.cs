﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsFine.API.Entities
{
    public class Fine
    {
        [Key]       
        public int IdFine { get; set; }

        [Required]
        public virtual FineType FineType { get; set; }

        [Required]
        public virtual Player Player { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [MaxLength(255)]
        public string Explaination { get; set; }

        //Add Later!
        //public virtual Player ReportingPlayer {get; set;}

        public bool IsMatch { get; set; }

        public bool IsApproved { get; set; }

        public bool IsPaid { get; set; }

        public DateTime DatePaid { get; set; }
        

        //public int PlayerIdPlayer { get; set; }
        //public int TeamIdTeam { get; set; }
        //public int ClubIdClub { get; set; }

    }
}
