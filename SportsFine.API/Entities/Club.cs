﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SportsFine.API.Entities
{
    public class Club
    {
        [Key]
        public int IdClub { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string UrlLogo { get; set; }

        [MaxLength(255)]
        public string Homepage { get; set; }

        //Used for relationships
        public List<Team> Teams { get; set; }

    }
}
