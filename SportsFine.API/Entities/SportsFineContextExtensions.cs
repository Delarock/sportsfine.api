﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System.Linq;

namespace SportsFine.API.Entities
{
    public static class SportsFineContextExtensions
    {

        public static void EnsureSeedDataForContext(this SportsFineContext context)
        {


            // first, clear the database.  This ensures we can always start 
            // fresh with each demo.  Not advised for production environments, obviously :-)

            //TEMP Removal to ensure that IDs are consistant. Doesnt seem to work if I add columns anyway...

            context.Fines.RemoveRange();
            context.Players.RemoveRange();
            context.FineTypes.RemoveRange();
            context.Teams.RemoveRange();
            context.Clubs.RemoveRange();
            context.SaveChanges();

            //init seed data
            if (!context.Clubs.Any())
            {

                var clubs = new List<Club>()
                {

                    new Club()
                    {
                        //IdClub = 0,
                        Name = "SK Jarl",
                        UrlLogo = "/temp.jpg",
                        Homepage = "www.skjarl.no",
                        Teams = new List<Team>
                        {
                            new Team()
                            {
                                //IdTeam = 0,
                                Name = "A-lag",
                            },
                            new Team()
                            {
                                //IdTeam = 1,
                                Name = "B-lag"
                            },
                            new Team()
                            {
                                //IdTeam = 2,
                                Name = "Gutt 2000"
                            }
                        }
                    }
                };

                context.Clubs.AddRange(clubs);
                context.SaveChanges();

            }

            if (!context.Players.Any())
            {
                var players = new List<Player>()
                {
                    new Player()
                    {
                            //IdPlayer = 0,
                            FirstName = "Morten",
                            LastName = "Hovdan",
                            DateOfBirth = new DateTimeOffset(new DateTime(1984, 5, 3)),
                            IsAdmin = true,
                            Team = context.Teams.FirstOrDefault(t => t.Name == "A-lag")
                    },
                    new Player()
                    {
                            //IdPlayer = 1,
                            FirstName = "Stian",
                            LastName = "Falnes",
                            DateOfBirth = new DateTimeOffset(new DateTime(1984, 9, 20)),
                            IsAdmin = true,
                            Team = context.Teams.FirstOrDefault(t => t.Name == "A-lag")
                    },
                    new Player()
                    {
                            //IdPlayer = 2,
                            FirstName = "Trygve Alexander",
                            LastName = "Lende",
                            DateOfBirth = new DateTimeOffset(new DateTime(1987, 11, 10)),
                            IsAdmin = true,
                            Team = context.Teams.FirstOrDefault(t => t.Name == "A-lag")
                    },
                    new Player()
                    {
                            //IdPlayer = 3,
                            FirstName = "Jan Erik",
                            LastName = "Haavik",
                            DateOfBirth = new DateTimeOffset(new DateTime(1989, 8, 27)),
                            IsAdmin = false,
                            Team = context.Teams.FirstOrDefault(t => t.Name == "A-lag")
                    },
                    new Player()
                    {
                            //IdPlayer = 4,
                            FirstName = "Håkon",
                            LastName = "Gabrielsen",
                            DateOfBirth = new DateTimeOffset(new DateTime(1993, 3, 11)),
                            IsAdmin = false,
                            Team = context.Teams.FirstOrDefault(t => t.Name == "A-lag")
                    }
                };
                context.Players.AddRange(players);
                context.SaveChanges();
            }

            if (!context.FineTypes.Any())
            {
                var finesTypes = new List<FineType>()
                {
                    new FineType()
                    {
                        //IdFineType = 0,
                        Title = "Uneccisary yellow card",
                        Description = "Yellow card from complaining on the referee, kicking away the ball or other stupid stuff.",
                        Amount = 50
                    },
                    new FineType()
                    {
                        //IdFineType = 2,
                        Title = "Late to practice",
                        Description = "Being late to practice without notifying the coach via SMS.",
                        Amount = 15
                    },
                    new FineType()
                    {
                        //IdFineType = 3,
                        Title = "Late to game",
                        Description = "Being late to a game without notifying the coach via SMS.",
                    },
                    new FineType()
                    {
                        //IdFineType = 4,
                        Title = "Shooting over the fence",
                        Description = "Sucking so bad that you not only miss the goal, you also shoot over the fence.",
                        Amount = 10,
                    },
                    new FineType()
                    {
                        //IdFineType = 7,
                        Title = "Uneccisary red card",
                        Description = "Red card from complaining on the referee, kicking away the ball or other stupid stuff.",
                        Amount = 100,
                    },
                    new FineType()
                    {
                        //IdFineType = 8,
                        Title = "Didn't show up at practice",
                        Description = "Being a no-show for practice without notifying coach via sms, or if the reason for not showing is not accepted.",
                        Amount = 30,
                    },
                    new FineType()
                    {
                        //IdFineType = 9,
                        Title = "Forgot equipment - game",
                        Description = "Forgetting any type of neccisarry equipment on match day.",
                        Amount = 50,
                    }
                };

                context.FineTypes.AddRange(finesTypes);
                context.SaveChanges();
            }

            if (!context.Fines.Any()) { 
                var fines = new List<Fine>()
                {
                    new Fine()
                    {
                        //IdFine = 0,
                        FineType = context.FineTypes.FirstOrDefault(f => f.Title == "Uneccisary yellow card"),
                        Player = context.Players.FirstOrDefault(p => p.LastName == "Hovdan"),
                        Date = new DateTime(2018,1,1),
                        IsApproved = true,
                        IsPaid = true,
                        DatePaid = new DateTime(2018,1,30),

                    },
                    new Fine()
                    {
                        //IdFine = 1,
                        FineType = context.FineTypes.FirstOrDefault(f => f.Title == "Late to practice"),
                        Player = context.Players.FirstOrDefault(p => p.LastName == "Hovdan"),
                        Date = new DateTime(2018,1,1),
                        IsApproved = true,
                        IsPaid = true,
                        DatePaid = new DateTime(2018,1,30),

                    },
                    new Fine()
                    {

                        FineType = context.FineTypes.FirstOrDefault(f => f.Title == "Late to game"),
                        Player = context.Players.FirstOrDefault(p => p.LastName == "Hovdan"),
                        Date = new DateTime(2018,1,10),
                        IsApproved = true,
                        IsPaid = false
                    },
                    new Fine()
                    {
                        //IdFine = 3,
                        FineType = context.FineTypes.FirstOrDefault(f => f.Title == "Uneccisary yellow card"),
                        Player = context.Players.FirstOrDefault(p => p.LastName == "Falnes"),
                        Date = new DateTime(2018,1,29),
                        IsApproved = false,
                        IsPaid = false
                    },
                    new Fine()
                    {
                        //IdFine = 4,
                        FineType = context.FineTypes.FirstOrDefault(f => f.Title == "Late to practice"),
                        Player = context.Players.FirstOrDefault(p => p.LastName == "Falnes"),
                        Date = new DateTime(2018,1,1),
                        IsApproved = true,
                        IsPaid = true,
                        DatePaid = new DateTime(2018,1,30)
                    },
                    new Fine()
                    {
                        //IdFine = 5,
                        FineType = context.FineTypes.FirstOrDefault(f => f.Title == "Shooting over the fence"),
                        Player = context.Players.FirstOrDefault(p => p.LastName == "Lende"),
                        Date = new DateTime(2018,1,1),
                        IsApproved = true,
                        IsPaid = true,
                        DatePaid = new DateTime(2018,1,30)
                    },
                    new Fine()
                    {
                        //IdFine = 6,
                        FineType = context.FineTypes.FirstOrDefault(f => f.Title == "Uneccisary yellow card"),
                        Player = context.Players.FirstOrDefault(p => p.LastName == "Lende"),
                        Date = new DateTime(2018,1,1),
                        IsApproved = true,
                        IsPaid = true,
                        DatePaid = new DateTime(2018,1,30)
                    },
                    new Fine()
                    {
                        //IdFine = 7,
                        FineType = context.FineTypes.FirstOrDefault(f => f.Title == "Shooting over the fence"),
                        Player = context.Players.FirstOrDefault(p => p.LastName == "Haavik"),
                        Date = new DateTime(2018,1,1),
                        IsApproved = true,
                        IsPaid = false
                    },
                    new Fine()
                    {
                        //IdFine = 8,
                        FineType = context.FineTypes.FirstOrDefault(f => f.Title == "Forgot equipment - game"),
                        Player = context.Players.FirstOrDefault(p => p.LastName == "Haavik"),
                        Date = new DateTime(2018,1,5),
                        IsApproved = true,
                        IsPaid = false
                    },
                    new Fine()
                    {
                        //IdFine = 9,
                        FineType = context.FineTypes.FirstOrDefault(f => f.Title == "Didn't show up at practice"),
                        Player = context.Players.FirstOrDefault(p => p.LastName == "Gabrielsen"),
                        Date = new DateTime(2018,1,5),
                        IsApproved = true,
                        IsPaid = false
                    }
                };

                context.Fines.AddRange(fines);
                context.SaveChanges();
            }   
        }

    }
}   
