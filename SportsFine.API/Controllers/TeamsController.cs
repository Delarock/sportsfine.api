﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SportsFine.API.Entities;
using SportsFine.API.Helpers;
using SportsFine.API.Services;
using AutoMapper;
using SportsFine.API.Models;
using Microsoft.AspNetCore.Cors;

namespace SportsFine.API.Controllers
{
    [Produces("application/json")]
    [Route("api/teams")]
    [EnableCors("MyPolicy")]
    public class TeamsController : Controller
    {
        private ISportsFineRepository _sportsFineRepository;

        public TeamsController(ISportsFineRepository sportsFineRepository)
        {
            _sportsFineRepository = sportsFineRepository;
        }

        // GET: api/teams
        [HttpGet]
        public IActionResult GetTeams()
        {
            var teamsFromRepo = _sportsFineRepository.GetTeams();

            //var teams = Mapper.Map<IEnumerable<TeamsDto>>(teamsFromRepo);

            return Ok(teamsFromRepo);
        }

        // GET: api/teams/5
        [HttpGet("{id}")]
        public IActionResult GetTeam(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var teamFromRepo = _sportsFineRepository.GetTeam(id);

            //var teams = Mapper.Map<IEnumerable<TeamsDto>>(teamsFromRepo);

            if (teamFromRepo == null)
            {
                return NotFound();
            }

            return Ok(teamFromRepo);
        }

        // POST: api/teams
        [HttpPost]
        public IActionResult PostTeam([FromBody] TeamDto teamDto)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Team team = new Team
            {
                Club = _sportsFineRepository.GetClub(teamDto.ClubId),
                Name = teamDto.Name
            }; 

            _sportsFineRepository.AddTeam(team);
            _sportsFineRepository.Save();
            teamDto.TeamId = team.IdTeam;
            return CreatedAtAction("PostTeam", teamDto);
        }

        // GET: api/teams/{teamId}
        [HttpGet("{id}/top10paid")]
        public IActionResult GetTeamTopTenContributor(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var players = _sportsFineRepository.GetPlayers();
            //var fines = _sportsFineRepository.GetFines();

            var result = _sportsFineRepository.TopTenContributingTeam(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        // GET: api/teams/{teamId}
        [HttpGet("{id}/top10owed")]
        public IActionResult GetTeamTopOutstanding(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var players = _sportsFineRepository.GetPlayers();
            //var fines = _sportsFineRepository.GetFines();

            var result = _sportsFineRepository.TopTenOutstandingTeam(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}