﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsFine.API.Services;
using SportsFine.API.Models;
using SportsFine.API.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Cors;

namespace SportsFine.API.Controllers
{
    [Produces("application/json")]
    [Route("api/players")]
    [EnableCors("MyPolicy")]
    public class PlayersController : Controller
    {
        private ISportsFineRepository _sportsFineRepository;

        public PlayersController(ISportsFineRepository sportsFineRepository)
        {
            _sportsFineRepository = sportsFineRepository;
        }
    

        // GET api/players
        [HttpGet ()]
        public IActionResult GetPlayers()
        {
            var playersFromRepo = _sportsFineRepository.GetPlayers();

            var players = Mapper.Map<IEnumerable<PlayerDto>>(playersFromRepo);

            return Ok(players);
        }

        // GET api/players/{id}
        [HttpGet("{id}")]
        public IActionResult GetPlayer(int id)
        {
            var playerFromRepo = _sportsFineRepository.GetPlayer(id);

            if(playerFromRepo == null)
            {
                return NotFound();
            }
            var player = Mapper.Map<PlayerDto>(playerFromRepo);

            return Ok(player);
        }

        // GET api/players/{id}/fines
        [HttpGet("{id}/fines")]
        public IActionResult GetPlayerFines(int id)
        {
            var playerFinesFromRepo = _sportsFineRepository.GetPlayerFines(id);

            if (playerFinesFromRepo == null || !playerFinesFromRepo.Any())
            {
                return NotFound();
            }
            var playerFines = Mapper.Map<IEnumerable<FineDto>>(playerFinesFromRepo);

            return Ok(playerFines);
        }

        // GET api/players/{id}/outstandingfines
        [HttpGet("{id}/outstandingfines")]
        public IActionResult GetPlayerOutstandingFines(int id)
        {
            var playerFinesFromRepo = _sportsFineRepository.GetPlayerOutstandingFines(id);

            if (playerFinesFromRepo == null || !playerFinesFromRepo.Any())
            {
                return NotFound();
            }
            var playerFines = Mapper.Map<IEnumerable<FineDto>>(playerFinesFromRepo);

            return Ok(playerFines);
        }

    }
}
