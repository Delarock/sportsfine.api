﻿using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SportsFine.API.Entities;
using SportsFine.API.Models;
using SportsFine.API.Services;
using System.Collections.Generic;

namespace SportsFine.API.Controllers
{
    [Produces("application/json")]
    [Route("api/fines")]
    [EnableCors("MyPolicy")]
    public class FinesController : Controller
    {
        private ISportsFineRepository _sportsFineRepository;

        public FinesController(ISportsFineRepository sportsFineRepository)
        {
            _sportsFineRepository = sportsFineRepository;
        }

        // GET: api/Fines/types
        [HttpGet("types")]
        public IActionResult GetFineTypes()
        {
            var fineTypesFromRepo = _sportsFineRepository.GetFineTypes();

            var fineTypes = fineTypesFromRepo;

            return Ok(fineTypes);
        }
        
        // GET: api/Fines
        [HttpGet]
        public IActionResult GetFines()
        {
            var finesFromRepo = _sportsFineRepository.GetFines();

            var fines = Mapper.Map<IEnumerable<FineDto>>(finesFromRepo);

            return Ok(fines);
        }

        // GET: api/Fines/5
        [HttpGet("{id}")]
        public IActionResult GetFine(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fine = _sportsFineRepository.GetFine(id);

            if (fine == null)
            {
                return NotFound();
            }

            return Ok(fine);
        }

        // POST: api/Fines
        [HttpPost]
        public IActionResult AddFine([FromBody] FineDto fineDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Verify Input values
            //TODO

            //Create new fine
            Fine fine = new Fine
            {
                FineType = _sportsFineRepository.GetFineType(fineDto.FineTypeId),
                Player = _sportsFineRepository.GetPlayer(fineDto.PlayerFinedId),
                Date = fineDto.Date,
                Explaination = fineDto.Explaination
            };

            _sportsFineRepository.AddFine(fine);
            _sportsFineRepository.Save();

            return CreatedAtAction("AddFine", fineDto);
        }

        // DELETE: api/Fines/5
        [HttpDelete("{id}")]
        public IActionResult DeleteFine(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fine = _sportsFineRepository.GetFine(id);
            if (fine == null)
            {
                return NotFound();
            }

            _sportsFineRepository.DeleteFine(fine);
            _sportsFineRepository.Save();

            return Ok(fine);
        }

        // PUT: api/Fines/5/approve
        [HttpPut("{id}/approve")]
        public IActionResult ApproveFine(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fine = _sportsFineRepository.GetFine(id);
            if (fine == null)
            {
                return NotFound();
            }

            _sportsFineRepository.ApproveFine(id);
            _sportsFineRepository.Save();

            fine = _sportsFineRepository.GetFine(id);

            return Ok(fine);
        }

        // PUT: api/Fines/5/paid
        [HttpPut("{id}/paid")]
        public IActionResult ApproveFinePayment(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fine = _sportsFineRepository.GetFine(id);
            if (fine == null)
            {
                return NotFound();
            }

            _sportsFineRepository.ApproveFinePayment(id);
            _sportsFineRepository.Save();

            fine = _sportsFineRepository.GetFine(id);

            return Ok(fine);
        }

        private bool FineExists(int id)
        {
            if(_sportsFineRepository.GetFine(id) == null)
            {
                return false;
            }
            return true;
        }
    }
}