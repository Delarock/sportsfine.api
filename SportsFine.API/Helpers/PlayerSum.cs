﻿using SportsFine.API.Entities;
using SportsFine.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsFine.API.Helpers
{
    public class PlayerSum
    {
        public int IdPlayer { get; set; }
        public string Name { get; set; }
        public int Sum { get; set; }
    }

    
}
