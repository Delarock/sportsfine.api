﻿using SportsFine.API.Entities;
using SportsFine.API.Helpers;
using System;
using System.Collections.Generic;

namespace SportsFine.API.Services
{
    public interface ISportsFineRepository
    {
        //Players
        IEnumerable<Player> GetPlayers();
        Player GetPlayer(int IdPlayer);
        void AddPlayer(Player player);
        void DeletePlayer(Player player);
        void UpdatePlayer(Player player);
        bool PlayerExists(int playerId);

        //Players for club/team


        //Fines
        IEnumerable<Fine> GetFines();
        Fine GetFine(int fineId);
        void AddFine(Fine fine);
        void UpdateFine(Fine fine); //burde kanskje inkludere int fineId?
        void ApproveFine(int fineId);
        void DeleteFine(Fine fine); //burde kanskje være int fineId?
        IEnumerable<FineType> GetFineTypes();
        FineType GetFineType(int fineTypeId);
        void ApproveFinePayment(int fineID);

        //Fines for club/team/player
        IEnumerable<Fine> GetPlayerFines(int playerId);
        IEnumerable<Fine> GetPlayerOutstandingFines(int playerId);
        Team GetTeam(int id);
        Club GetClub(int id);
        IEnumerable<Team> GetTeams();
        void AddTeam(Team team);
        IEnumerable<Fine> GetTeamFines(int teamId);
        IEnumerable<Fine> GetClubFines(int clubId);
        IEnumerable<PlayerSum> TopTenContributingTeam(int teamId);
        IEnumerable<PlayerSum> TopTenOutstandingTeam(int teamId);


        //General
        bool Save();
        
    }
}
