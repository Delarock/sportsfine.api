﻿using SportsFine.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using SportsFine.API.Helpers;
using Microsoft.EntityFrameworkCore;

namespace SportsFine.API.Services
{
    public class SportsFineRepository : ISportsFineRepository
    {
        private SportsFineContext _context;


        //Define context
        public SportsFineRepository(SportsFineContext context)
        {
            _context = context;
        }

        //Players
        public IEnumerable<Player> GetPlayers()
        {
            return _context.Players.OrderBy(a => a.FirstName).ThenBy(a => a.LastName);
        }
        public Player GetPlayer(int playerId)
        {
            return _context.Players.FirstOrDefault(a => a.IdPlayer == playerId);
        }
        public void AddPlayer(Player player)
        {
            player.IdPlayer = player.IdPlayer;
            _context.Players.Add(player);
        }
        public void DeletePlayer(Player player)
        {
            _context.Players.Remove(player);
        }
        public void UpdatePlayer(Player player)
        {
            throw new NotImplementedException();
        }
        public bool PlayerExists(int playerId)
        {
            return _context.Players.Any(a => a.IdPlayer == playerId);
        }

        //Players for clubs/teams
        
        //Fines
        public Fine GetFine(int fineId)
        {
            return _context.Fines
                .Include(f => f.Player)
                .Include(f => f.FineType)
                .FirstOrDefault(b => b.IdFine == fineId);
                
        }
        public IEnumerable<Fine> GetFines()
        {
            return _context.Fines
                .Include(f => f.Player)
                .Include(f => f.FineType);
        }

        public IEnumerable<FineType> GetFineTypes()
        {
            return _context.FineTypes;
        }
        public FineType GetFineType(int fineTypeId)
        {
            return _context.FineTypes.FirstOrDefault(b => b.IdFineType == fineTypeId);
        }
        public void AddFine(Fine fine)
        {
            _context.Add(fine);
        }
        public void DeleteFine(Fine fine)
        {
            _context.Fines.Remove(fine);
        }
        public void UpdateFine(Fine fine)
        {
            throw new NotImplementedException();
        }
        public void ApproveFine(int fineId)
        {
            _context.Fines.FirstOrDefault(a => a.IdFine == fineId).IsApproved = true;
        }
        public void ApproveFinePayment(int fineId)
        {
            _context.Fines.FirstOrDefault(a => a.IdFine == fineId).IsPaid = true;
            _context.Fines.FirstOrDefault(a => a.IdFine == fineId).DatePaid = DateTime.Now.Date;
        }


        //Fines for club/team/player

        public IEnumerable<Fine> GetPlayerFines(int playerId)
        {
            return _context.Fines
                .Include(f => f.Player)
                .Include(f => f.FineType)
                .Where(b => b.Player.IdPlayer == playerId).OrderBy(b => b.Date).ToList();
        }
        public IEnumerable<Fine> GetPlayerOutstandingFines(int playerId)
        {
            return _context.Fines
                .Include(f => f.Player)
                .Include(f => f.FineType)
                .Where(b => b.Player.IdPlayer == playerId && b.IsPaid == false).OrderBy(b => b.Date).ToList();
        }

        public IEnumerable<Team> GetTeams()
        {
            return _context.Teams
                .Include(t => t.Players);
        }
        public Team GetTeam(int id)
        {
            return _context.Teams.FirstOrDefault(b => b.IdTeam == id);
        }
        public Club GetClub(int id)
        {
            return _context.Clubs.FirstOrDefault(b => b.IdClub == id);
        }
        public void AddTeam(Team team)
        {
            _context.Teams.Add(team);
        }

        public IEnumerable<Fine> GetTeamFines(int teamId)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<Fine> GetClubFines(int clubId)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<PlayerSum> TopTenContributingTeam(int teamId)
        {
            var players = _context.Players
                .Where(p => p.Team.IdTeam == teamId).OrderBy(p => p.IdPlayer)
                .Include(p => p.Team);
            var fines = _context.Fines.
                Where(f => f.Player.Team.IdTeam == teamId).OrderBy(f => f.IdFine)
                .Include(f => f.FineType)
                .Include(f => f.Player);
            var sumOfPlayerFines = new List<PlayerSum>();

            foreach (Player player in players)
            {
                sumOfPlayerFines.Add(new PlayerSum
                {
                    IdPlayer = player.IdPlayer,
                    Name = player.FirstName + " " + player.LastName,
                    Sum = 0
                });
            }

            foreach (Fine fine in fines)
            {
                if (sumOfPlayerFines.FirstOrDefault(ps => ps.IdPlayer == fine.Player.IdPlayer) != null && fine.IsPaid && fine.IsApproved)
                {
                    sumOfPlayerFines.FirstOrDefault(ps => ps.IdPlayer == fine.Player.IdPlayer).Sum += fine.FineType.Amount;
                }
            }

            return sumOfPlayerFines.OrderByDescending(ps => ps.Sum).ThenBy(ps => ps.Name);
        }
        public IEnumerable<PlayerSum> TopTenOutstandingTeam(int teamId)
        {
            var players = _context.Players
                .Where(p => p.Team.IdTeam == teamId).OrderBy(p => p.IdPlayer)
                .Include(p => p.Team);
            var fines = _context.Fines.
                Where(f => f.Player.Team.IdTeam == teamId).OrderBy(f => f.IdFine)
                .Include(f => f.FineType)
                .Include(f => f.Player);
            var sumOfPlayerFines = new List<PlayerSum>();

            foreach (Player player in players)
            {
                sumOfPlayerFines.Add(new PlayerSum
                {
                    IdPlayer = player.IdPlayer,
                    Name = player.FirstName + " " + player.LastName,
                    Sum = 0
                });
            }

            foreach (Fine fine in fines)
            {
                if (sumOfPlayerFines.FirstOrDefault(ps => ps.IdPlayer == fine.Player.IdPlayer) != null && !fine.IsPaid && fine.IsApproved)
                {
                    sumOfPlayerFines.FirstOrDefault(ps => ps.IdPlayer == fine.Player.IdPlayer).Sum += fine.FineType.Amount;
                }
            }

            return sumOfPlayerFines.OrderByDescending(ps => ps.Sum).ThenBy(ps => ps.Name);
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }


    }
}
